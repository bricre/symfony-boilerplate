Bricre - Symfony API Platform - Boilerplate Project

This is a boilerplate project based on [API Platform](https://api-platform.com/) to quickly get you quickly started with 
a ready-to-work-on micro service project.

# Prerequistes

 - [Docker Destop](https://docs.docker.com/desktop/)
 - [Docker Compose](https://docs.docker.com/compose/install/)
 - [Mutagen](https://mutagen.io)

# Setup

To run `composer update` in Docker environment, it requires a large amount of memory, please increase [Memory
 resource](https://docs.docker.com/docker-for-mac/), other wise the update may fail without error shown. 

```sh
docker run --rm -it -v $(pwd):/app composer create-project bricre/symfony-boilerplate  .

docker run --rm -it -v $(pwd):/app composer php bin/console assets:install --relative

docker-compose up -d

mutagen project start

```

---

Update `.env` file

```ini
MICROSERVICE=test
```

The `MICROSERVICE` environment variable is used to determine path which is microservice is going to be routed.

In this example we set it to be `test` so the service will be accessible via `/api/test/`, as per setup in below.

If we set it `user`, the service will then be accessible via `/api/user` (correct Kong configs need to be applied accordingly.)

---

Since this is a MicroService project, we put [Kong](https://konghq.com/) Gateway to the front of all micro services, 
no matter it is a PHP based API service (such as this project), or a ReactJS based front end interface. All traffics 
will come in through Kong.

To put this project running, we need to run the following commands (port 8001 is the default Kong admin api port):

```sh
# Set service and route for Konga (a Kong web admin ui)
curl -i -X POST \
  --url http://localhost:8001/services/ \
  --data 'name=konga' \
  --data 'host=konga' \
  --data 'port=1337'

curl -i -X POST \
  --url http://localhost:8001/services/konga/routes \
  --data 'name=konga' \
  --data 'paths[]=/konga/' 

# Set service and routes for the microservice (nginx) we are working on
curl -i -X POST \
  --url http://localhost:8001/services/ \
  --data 'name=test' \
  --data 'host=nginx'

curl -i -X POST \
  --url http://localhost:8001/services/test/routes \
  --data 'name=test-secured' \
  --data 'paths[]=/api/test/' \
  --data 'strip_path=false' \
  --data 'preserve_host=true'

# _profiler and _wdt are for dev environment only
curl -i -X POST \
  --url http://localhost:8001/services/test/routes \
  --data 'name=test-public' \
  --data 'paths[]=/api/test/_profiler' \
  --data 'paths[]=/api/test/_wdt' \
  --data 'paths[]=/api/test/login' \
  --data 'paths[]=/api/test/docs' \
  --data 'strip_path=false' \
  --data 'preserve_host=true'

# The 'assets' directory is only used in dev environment for web profiler to correctly work
# Notice we delirately put strip_path=true, this is the only way I can get it working easily.
curl -i -X POST \
  --url http://localhost:8001/services/test/routes \
  --data 'name=test-assets' \
  --data 'paths[]=/api/test/assets/' \
  --data 'strip_path=true' \
  --data 'preserve_host=true'
```

Read [Kong's Getting Started](https://docs.konghq.com/getting-started-guide/2.1.x/expose-services/) about how to 
customise the routes.

You can also visit [http://localhost:8000/konga/](http://localhost:8000/konga/) with username `kong` and password `password` see [docker/kong_user.js](docker/konga_user.js)

## Mutagen
Because Symfonly heavily utilises auto-wireing and auto-configuration, in dev environment the whole Dependency Injection 
system will be re-compiled each time when a source code is being updated. This causes a performance issue in dev 
environment due to the amount of files being accessed, especially for Docker under Mac & Windows platforms.

Until this performance issued getting fixed by Docker, we use [Mutagen](https://mutagen.io/) to improve file system performance.

In [docker-compose.yaml](docker/docker-compose.yaml) we sepcify a container_name, which is being used later in [mutagen.yml](mutagen.yml) see `sync:codebase:beta`.

Should you have multiple projects running at the same time, remember to change the container_name to avoid confliction.

**Notice:** For better system performance, we ignored `vendor/` folder, it will only be flushed when you start mutagen project.

If you installed new composer modules and found it not included in your project, just run the following script to restart mutagen:

```sh
mutagen project terminate
mutagen project start
```

# Development

Website will be accessible through http://localhost:8000/api/test now, you should see Symfony default welcome screen.

##  Database

Connect to the database with your prefered desktop tool with following settings:

Connection with SSH:
* host: 127.0.0.1
* port: 2222
* username: root
* password: root

Connection to the database:
* host: db
* username: root
* password: password
* database: db_name 

After creating db schema, run the following script to 
[generate relevant Doctrine files](https://symfony.com/doc/current/doctrine/reverse_engineering.html):

```sh
docker-compose exec php /var/www/html/bin/console doctrine:mapping:import "App\Entity" annotation --path=src/Entity
```

The code generated by above line doesn't create `Repository` class, to do this you need to add a custom Entity
 reference in the PHPDoc for each new Entity model class.
 
Say I have a class of `User` entity created, I need to change the class comment from

```php
use Doctrine\ORM;
/**
 * TestUser
 *
 * @ORM\Table(name="user")
 * @ORM\Entity
 */
class User{}
``` 

to 

```php
use Doctrine\ORM;
/**
 * TestUser
 *
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User{}
```

And run

```sh
docker-compose exec php /var/www/html/bin/console make:entity --regenerate
```

This will generate a Repository class for you to work on.

## API development

Once you add `@ApiResource()` to an Entity, it will automatically become an API resouce.

```php
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM;
/**
 * TestUser
 * @ApiResource()
 * 
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User{}
```

We can visit http://localhost:8000/api/test/docs to interact with the generated API enpoints.

Please read [api-platform.com](https://api-platform.com/) for more api development related guides. 

# Debugging
The WebProfiler toolbar at the bottom of Swagger UI is a good point to start with your diagnosing.

xDebug is installed in PHP, and it should work with default settings.

If your debug session doesn't start automatically and you are sure you've set everything correctly. Try to restart PHPStorm.
I found it always helps, PHPStorm some times stop listening to port 9000 even if you toggled the debug button. Only restart
solves the problem.