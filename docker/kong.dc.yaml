version: '3.7'

volumes:
  kong_data: {}

services:
  kong-migrations:
    image: "${KONG_DOCKER_TAG:-kong:latest}"
    command: kong migrations bootstrap
    depends_on:
      - kong-db
    environment:
      KONG_DATABASE: postgres
      KONG_PG_DATABASE: ${KONG_PG_DATABASE:-kong}
      KONG_PG_HOST: kong-db
      KONG_PG_USER: ${KONG_PG_USER:-kong}
      KONG_PG_PASSWORD_FILE: /run/secrets/kong_postgres_password
    secrets:
      - kong_postgres_password
    restart: on-failure

  kong-migrations-up:
    image: "${KONG_DOCKER_TAG:-kong:latest}"
    command: kong migrations up && kong migrations finish
    depends_on:
      - kong-db
    environment:
      KONG_DATABASE: postgres
      KONG_PG_DATABASE: ${KONG_PG_DATABASE:-kong}
      KONG_PG_HOST: kong-db
      KONG_PG_USER: ${KONG_PG_USER:-kong}
      KONG_PG_PASSWORD_FILE: /run/secrets/kong_postgres_password
    secrets:
      - kong_postgres_password
    restart: on-failure

  kong:
    image: "${KONG_DOCKER_TAG:-kong:latest}"
    user: "${KONG_USER:-kong}"
    depends_on:
      - kong-db
    environment:
      KONG_ADMIN_ACCESS_LOG: /dev/stdout
      KONG_ADMIN_ERROR_LOG: /dev/stderr
      KONG_ADMIN_LISTEN: '0.0.0.0:8001'
      KONG_CASSANDRA_CONTACT_POINTS: kong-db
      KONG_DATABASE: postgres
      KONG_PG_DATABASE: ${KONG_PG_DATABASE:-kong}
      KONG_PG_HOST: kong-db
      KONG_PG_USER: ${KONG_PG_USER:-kong}
      KONG_PROXY_ACCESS_LOG: /dev/stdout
      KONG_PROXY_ERROR_LOG: /dev/stderr
      KONG_PG_PASSWORD_FILE: /run/secrets/kong_postgres_password
    secrets:
      - kong_postgres_password
    ports:
      - "8000:8000/tcp"
      - "127.0.0.1:8001:8001/tcp"
      - "8443:8443/tcp"
      - "127.0.0.1:8444:8444/tcp"
    healthcheck:
      test: ["CMD", "kong", "health"]
      interval: 10s
      timeout: 10s
      retries: 10
    restart: on-failure

  kong-db:
    image: postgres:9.5-alpine
    environment:
      POSTGRES_DB: ${KONG_PG_DATABASE:-kong}
      POSTGRES_USER: ${KONG_PG_USER:-kong}
      POSTGRES_PASSWORD_FILE: /run/secrets/kong_postgres_password
    secrets:
      - kong_postgres_password
    healthcheck:
      test: ["CMD", "pg_isready", "-U", "${KONG_PG_USER:-kong}"]
      interval: 30s
      timeout: 30s
      retries: 3
    restart: on-failure
    stdin_open: true
    tty: true
    volumes:
      - kong_data:/var/lib/postgresql/data

  konga-prepare:
    image: pantsel/konga
    depends_on:
      - kong-db
    environment:
      NODE_ENV: production
      DB_ADAPTER: postgres
      DB_URI: "postgres://kong:kong@kong-db:5432/kong"
      DB_USER: kong
      DB_PASSWORD: kong
      NO_AUTH: "true"
      TOKEN_SECRET: "e71829c351aa4242c2719cbfbe671c09"
    command:
      - -c
      - prepare
    restart: on-failure

  konga:
    image: pantsel/konga
    depends_on:
      - kong
    environment:
      NODE_ENV: production
      DB_ADAPTER: postgres
      DB_URI: "postgres://kong:kong@kong-db:5432/kong"
      DB_USER: kong
      DB_PASSWORD: kong
      NO_AUTH: "false"
      TOKEN_SECRET: "e71829c351aa4242c2719cbfbe671c09"
      KONGA_SEED_USER_DATA_SOURCE_FILE: "/tmp/konga_user.js"
      KONGA_SEED_KONG_NODE_DATA_SOURCE_FILE: "/tmp/konga_node.js"
    volumes:
      - ./konga_user.js:/tmp/konga_user.js
      - ./konga_node.js:/tmp/konga_node.js

secrets:
  kong_postgres_password:
    file: ./KONG_POSTGRES_PASSWORD