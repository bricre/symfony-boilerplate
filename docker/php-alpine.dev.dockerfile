FROM hermsi/alpine-fpm-php
RUN apk add --no-cache $PHPIZE_DEPS \
    && pecl install xdebug \
    && docker-php-ext-enable xdebug \
    && apk del $PHPIZE_DEPS \
    && docker-php-source delete \
    && rm -rf /tmp/* /var/cache/apk/*